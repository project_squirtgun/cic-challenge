
/**
 * @author lukasdraschkowitz
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
 
@Path("/movies")
public class MovieWebService {
	
	//data api url containing movie data
	static String api_url = "https://data.sfgov.org/resource/wwmu-gmzc.json";
 
	  //return list of movies in json
	  @GET
	  @Produces("application/json")
	  public Response getMovies() throws JSONException {

		//receive json String from url and save as result
		String result = ""; 
		try {
			//Queries using SODA and SoQL
			//uses "%20" instead of space in path arguments
			String api_url_soql = api_url+"?$select=title,%20locations"; 
			result = IOUtils.toString(new URL(api_url_soql), Charset.forName("UTF-8"));
		} catch (IOException e) {e.printStackTrace();}
		
		return Response.status(200).entity(result).build();
	  }
 
	  //return list of movies in json filtering by title argument
	  @Path("/{title}")
	  @GET
	  @Produces("application/json")
	  public Response getMoviesWithParameter(@PathParam("title") String title) throws JSONException {
		  
		//add url encoded parameter to data api url
		//Queries using SODA and SoQL
		//replace spaces with "%20" in path arguments (percent encoding), "=" with %3D and ' with %27
		String api_url_p = api_url+"?$query=SELECT title, locations WHERE title = %22".replaceAll(" ", "%20") + title +"%22";

		//receive json String from url and save as result
		String result = ""; 
		try {
			result = IOUtils.toString(new URL(api_url_p), Charset.forName("UTF-8"));
		} catch (IOException e) {e.printStackTrace();}

		return Response.status(200).entity(result).build();
	  }
}