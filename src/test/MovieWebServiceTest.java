
/**
 * @author lukasdraschkowitz
 */
 

package test;
 
import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.hasItems;
 
import org.junit.Test;
 
public class MovieWebServiceTest {
	@Test
	public void testUserFetchesSuccess() {
		expect().
			body("locations",hasItems("Epic Roasthouse (399 Embarcadero)","City Hall", "Forest Hill Station- MUNI")). 
			body("title", hasItems("Zodiac", "Edtv", "Dirty Harry")).
		when().
			get("/CIC-Challenge/ServiceLukas/movies");
	}
 
	@Test
	public void testUserNotFound() {
		expect().
			body("locations",hasItems("Duboce Park","Oasis Nightclub (298 11th St)")). 
			body("title", hasItems("Summertime")).
		when().
			get("/CIC-Challenge/ServiceLukas/movies/Summertime");
	}
}


//package test; 
//
//import static org.junit.Assert.*;
//
//import javax.ws.rs.core.Application;
//
//import org.glassfish.jersey.server.ResourceConfig;
//import org.glassfish.jersey.test.JerseyTest;
//import org.junit.Assert;
//import org.junit.Test;
//
//import webservice.MovieWebService;
//
//public class MovieWebServiceTest extends JerseyTest {
//		 
//		    @Override
//		    protected Application configure() {
//		        return new ResourceConfig(MovieWebService.class);
//		    }
//		 
////		    @Test
////		    public void test() {
////		        final String hello = target("hello").request().get(String.class);
////		        assertEquals("Hello World!", hello);
////		    }
//		    
//		    @Test
//		    public void ordersFixedPathTest() {
//		        String response = target("/ServiceLukas/movies/").request().get(String.class);
//		        Assert.assertTrue("[{\"locations\":\"Epic Roasthouse".equals(response.substring(0, 29)));
//		    }
//		    
//		    @Test
//		    public void ordersPathParamTest() {
//		        String response = target("/ServiceLukas/movies/Summertime").request().get(String.class);
//		        Assert.assertTrue("[{\"locations\":\"20th St and Church".equals(response.substring(0, 32)));
//		    }
//		
//}